import App from "./App"


describe('App component ',()=>{
    let wrapper
    beforeEach(() => {
        wrapper = shallow(
            <App/>
        )
    })
    it('renders correctly',()=>{
        expect(wrapper.find('[data-test="App"]').length).toEqual(1)
        expect(!!wrapper).toBeTruthy()
    })
})