import React from 'react';
import './App.css';
import Dashboard from './dashboard/Dashboard';

function App() {
  return (
    <div className="App" data-test="App">
      <Dashboard/>
    </div>
  );
}

export default App;
