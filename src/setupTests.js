
import React from 'react'
import Enzyme, { shallow, render, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

//FIX ISSUE : https://github.com/facebook/jest/issues/3126#issuecomment-289283760
import 'babel-polyfill'


global.Math.random = () => 0.5
// import * as sharedLogic from "./views/Analytics/sharedLogic";

// React 16 Enzyme adapter
Enzyme.configure({ adapter: new Adapter() })

/**
 * FIX ISSUE : https://github.com/jerairrest/react-chartjs-2/issues/155#issuecomment-313970624
 */
// jest.mock('react-chartjs-2', () => ({
//   ...jest.requireActual('react-chartjs-2'),
//   Doughnut: () => null,
//   Line: () => null,
//   Bar: () => null,
//   Pie: () => null,
// }))
/**
 * mock useHistory hooks from react-router-dom
 */

// jest.mock('react-router-dom', () => ({
//   ...jest.requireActual('react-router-dom'),
//   useHistory: () => ({
//     push: jest.fn(),
//   }),
// }))

/**
 * --------------| global helpers |--------------
 */

/**
 * ignore errors from the console when needed
 */
global.ignoreErrors = () => (console.error = () => {})

/**
 * initialState of the redux store
 */
global.initialState = {
  common: {
    interval: {},
    analytics: null,
    pending: false,
    comments: [],
    diagnostics: {
      buckets: [],
    },
    tableLimit: 4,
    offset: 0,
    selectedDiagnostics: null,
  },
  user: {
    token: null,
    loginSent: false,
    loginErrors: [],
  },
  users: {
    list: [],
    offset: 0,
    tableLimit: 4,
    selectedUser: null,
    total: 0,
    addingUserErrors: [],
    addingUserSent: false,
    successAddingUser: false,

    updatingUserErrors: [],
    updatingUserSent: false,
    successUpdatingUser: false,
  },
  passwordRelated: {
    forgotPasswordErrors: [],
    forgotPassSent: false,
    successForgotPassword: null,
    resetPasswordErrors: [],
    resetPassSent: false,
    successResetPassword: null,
    changePasswordErrors: [],
    changePassSent: false,
    successChangePassword: null,
  },
  errors: [],
}

/**
 * tired of importing react at each test ?
 */
global.React = React

// Make Enzyme functions available in all test files without importing
global.shallow = shallow
global.render = render
global.mount = mount

// Fix ISSUE : https://stackoverflow.com/questions/54382414/fixing-react-leaflet-testing-error-cannot-read-property-layeradd-of-null
var createElementNSOrig = global.document.createElementNS
global.document.createElementNS = function (namespaceURI, qualifiedName) {
  if (namespaceURI === 'http://www.w3.org/2000/svg' && qualifiedName === 'svg') {
    var element = createElementNSOrig.apply(this, arguments)
    element.createSVGRect = function () {}
    return element
  }
  return createElementNSOrig.apply(this, arguments)
}
