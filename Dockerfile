### STAGE 1: Build ###
FROM node:12.16-alpine AS build
WORKDIR /usr/src/app/
COPY package.json yarn.lock ./
RUN yarn
COPY . ./
RUN apk add git
RUN GENERATE_SOURCEMAP=false yarn build

### STAGE 2: Run ###
FROM nginx:1.17-alpine
RUN rm -rf /usr/share/nginx/html/*
COPY --from=build /usr/src/app/build/ /usr/share/nginx/html
EXPOSE 80
WORKDIR /usr/share/nginx/html
COPY run.sh ./
RUN chmod +x run.sh
CMD ["/bin/sh", "-c", "env && /usr/share/nginx/html/run.sh && nginx -g \"daemon off;\""]
